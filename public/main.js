window.onload = function(){
  const {DeckGL, GeoJsonLayer} = deck;

  window.deckGL = new DeckGL({
    mapboxApiAccessToken: 'pk.eyJ1Ijoic2FuZHJhbWFwIiwiYSI6ImNrMG50MXhpYzAyejczZHM4czljaTd3N3IifQ.nJ5wF8_bc29VWSTMaAWHXA',
    mapStyle: 'mapbox://styles/mapbox/light-v9',
    latitude: 59.334,
    longitude: 18.063,
    zoom: 8,
    maxZoom: 16,
    pitch: 45,
    layers: []
  });

  window.data = {
    values: [],
    ranges: [],
    variable: 'car_time_peak',
    direction: 'from',
    zone: 712031
  }

  $("#chosen-variable").change(function(e){
    var newVar = e.target.value;
    if (newVar == 'no-var') return;
    $("#chosen-variable option[value='no-var']").attr("disabled","disabled");
    $("#direction-container").show();
    window.data.variable = newVar;
    getZone();
  });

  $('#exploring').click(function(e) {
    $('#welcome-message').hide();
  });

  $('input[type="radio"][name="direction"]').change(function(){
    window.data.direction = this.value;
    getZone();
  });
}

const DEFAULT_COLORS = [[1, 152, 189],[73, 227, 206],[216, 254, 181],[254, 237, 177],[254, 173, 84],[209, 55, 78]];
const CO2_COLORS = [[116,196,118],[254,229,217],[252,174,145],[251,106,74],[203,24,29]];

const MESSAGES = {
  ttr_peak: "Values between 0 and 1 indicate a 'Transit Paradise'. Above 1, transit is less competitive than car",
  ttr_offpeak: "Values between 0 and 1 indicate a 'Transit Paradise'. Above 1, transit is less competitive than car",
  co2: 'Values under 1 indicate that the trips from the origin zone produce less CO2 than the national average. The extrusion represents the total car demand for the O/D pair'
}

function varMessage() {
  return MESSAGES[window.data.variable] || '';
}

function rgbToHex(r, g, b) {
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function zoneProps(feature) {
  return window.data.values.filter(e => e.zone_id == feature.properties.zone_id)[0];
}

function zoneVal(feature) {
  return Math.round(zoneProps(feature).val * 100) / 100
}

function calcFillColor(feature) {
  val = zoneVal(feature);

  const defaultOpacity = window.data.variable == 'co2' ? 255 : 0;
  var color = [255,255,255,defaultOpacity];
  for (var i= 0; i < window.data.ranges.length; i++) {
    var max = window.data.ranges[i];
    if (val <= max && val > 0){
      color = window.data.colors[i-1];
      break;
    }
  }
  return color;
}

function handleHover (args) {
  const tooltip = document.getElementById('tooltip');

  if (args.object) {
    tooltip.style.top = `${args.y}px`;
    tooltip.style.left = `${args.x}px`;
    tooltip.innerHTML = "Zone: " + args.object.properties.name + "<br>Value: " + zoneVal(args.object);
  } else {
    tooltip.innerHTML = '';
  }
}
function getZone(){
  var url='/query';

  if (window.data.variable == 'ttr_peak') {
    url += '/ttr?tod=peak';
  } else if (window.data.variable == 'ttr_offpeak') {
    url += '/ttr?tod=offpeak';
  } else if(window.data.variable=='co2'){
    url+= '/emissions?';
  } else {
    url += '/variable?variable='+window.data.variable;
  }

  url += '&zone='+window.data.zone+'&direction='+window.data.direction;

  $.getJSON(url, function (data) {
    window.data.values=data;
    var values = [];
    data.forEach(function(el) {values.push(el.val)});

    var series = new geostats(values);
    series.setPrecision(2);
    window.data.ranges = series.getClassJenks(5);

    let layerOpts = {
      data: '/zones.geojson',
      pickable: true,
      opacity: 0.3,
      stroked: true,
      filled: true,
      getFillColor: calcFillColor,
      getLineColor:  [0,0,0],
      getLineWidth: (f) => {
        if (f.properties.zone_id == window.data.zone){
          return 150;
        } else {
          return 10;
        }
      },
      onClick: (event) => {
        const zone = event.object.properties.zone_id;
        const name = event.object.properties.name;
        selectZone(zone,name);
        return true;
      },
      onHover: handleHover,
      updateTriggers: {
        getFillColor: [window.data.values],
        getElevation: [window.data.values],
        getLineWidth: [window.data.values]
      }
    };

    if (window.data.variable == 'co2') {
      window.data.ranges[1] = 1;
      series.setRanges(window.data.ranges);
      window.data.colors = CO2_COLORS;

      // new layer opts
      layerOpts.extruded = true;
      layerOpts.wireframe = true;
      layerOpts.opacity = 1;
      layerOpts.elevationScale = 100;
      layerOpts.getElevation = function(feature) {
        return zoneProps(feature).elevation || 0;
      };
      //

    } else {
      window.data.colors = DEFAULT_COLORS;
    }

    series.setColors(window.data.colors.map(c=>rgbToHex(c[0],c[1],c[2])));

    $("#legend").html(series.getHtmlLegend());
    $("#var-message").html(varMessage());

    const layer = new GeoJsonLayer(layerOpts);
    window.deckGL.setProps({layers:[layer]});
  });
}

function selectZone(zone, name) {
  window.data.zone = zone;
  $("#selected-zone").html("Selected zone: <br>" + name);
  getZone();
}

