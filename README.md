# Stockholm Trips Explorer

Visualization of SAMPERS simulated data, with focus on sustanaible transportation.

Final project for AG2417 Web and Mobile GIS, KTH.

Public URL: [https://stockholm-trips-explorer-dot-webgis-251406.appspot.com](https://stockholm-trips-explorer-dot-webgis-251406.appspot.com)

AUTHORS:
- Nick Manolis
- Sandra Rangelova
- Bruno Salerno
