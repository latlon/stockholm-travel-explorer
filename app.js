const express = require('express')
const app = express ()
const port = 8080;
app.use(express.static('public'))
const bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing post data that has json format
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET ,POST ,PUT ,DELETE , OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type')
        ;
    next();
});
const { Pool } = require('pg');
const pool = new Pool({
    user: 'postgres',
    //host: 'localhost',
    host: '172.17.0.1',
    database: 'project',
    password: 'kth',
    //port: 3306,
    port: 5432
});

app.get('/zones.geojson', (req, res) => {
  const query =`
  SELECT json_build_object(
      'type', 'FeatureCollection',
      'crs',  json_build_object(
          'type',      'name',
          'properties', json_build_object(
              'name', 'EPSG:4326'
          )
      ),
      'features', json_agg(
          json_build_object(
              'type',       'Feature',
              'geometry',   ST_AsGeoJSON(geom)::json,
              'properties', json_build_object(
                  'zone_id', zone_id,
                  'name', name
              )
          )
      )
  )
  FROM zones;`
  pool.query(query, (err, dbResponse) => {
    if (err) console.log(err);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(dbResponse.rows[0].json_build_object);
  });
});

function zoneOrigAndDest (query) {
  const zone = query.zone;
  const orig = query.direction == 'from' ? 'orig_id' : 'dest_id';
  const dest = query.direction == 'from' ? 'dest_id' : 'orig_id';
  return [zone, orig, dest];
}

app.get('/query/variable', (req, res) => {
    const [zone, orig, dest] = zoneOrigAndDest(req.query);

    const variable = req.query.variable;
    pool.query(`select ${dest} as zone_id, ${variable} as val from demand where ${orig}=$1`,[zone], (err, dbResponse) => {
        if (err) console.log(err);
        // console.log ( dbResponse.rows );
        // here dbResponse is available , your data processing logic goes here
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.send(dbResponse.rows);
    });
});

app.get('/query/ttr', (req, res) => {
  const [zone, orig, dest] = zoneOrigAndDest(req.query);
  const timeOfDay = req.tod == 'offpeak' ? 'offpeak' : 'peak';

  const query = `
    select ${dest} as zone_id, ((2 * aux_time_${timeOfDay} + tot_wait_time_${timeOfDay} + invehicle_time_${timeOfDay}) / (car_time_${timeOfDay} + 5)) as val
    from demand
    where car_time_${timeOfDay} is not null
    and ${orig}=$1`;

  pool.query(query, [zone], (err, dbResponse) => {
    if (err) console.log(err);
    res.send(dbResponse.rows);
  });
});

app.get('/query/emissions', (req, res) => {
    const [zone, orig, dest] = zoneOrigAndDest(req.query);

    const query = `
      select ${dest} as zone_id, ((car_distance*120)/(4657/3)) as val,
      (car_demand_business + car_demand_other + car_demand_work) as elevation
      from demand
      where ${orig}=$1`;

    pool.query(query, [zone], (err, dbResponse) => {
      if (err) console.log(err);
      res.send(dbResponse.rows);
    });
  });

app.get('/',(req, res) => { res.sendFile(__dirname + '/index.html'); })
app.listen(port, () => console.log(`App listening on port ${port}!`))
